package main

import (
	"fmt"
	"github.com/jasonlvhit/gocron"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"gitlab.com/buddyspencer/loghammer"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type Config struct {
	Elasticsearch 	string
	Schedule 	  	string
	Logfile 	  	string
	Logdate			bool
	Indices 		[]Index
	Test 			bool
}

type Index struct {
	Name			string
	Dateformat		string
	Keep			string
}

var (
	config *Config
	ok bool
	nums = regexp.MustCompile("[0-9]+")
	abc = regexp.MustCompile("[A-Z,a-z]+")
	todelete = []string{}
)

func getPeriod(period string) float64 {
	p := nums.FindString(period)
	i := abc.FindString(period)

	pf, err := strconv.ParseFloat(p, 64)
	if err != nil {
		loghammer.Panic(err)
	}
	switch strings.ToUpper(i) {
	case "D":
		return pf
	case "M":
		return pf * 30
	case "Y":
		return pf * 364
	default:
		loghammer.Panicf("Doesn't recognize %s", i)
	}

	return 0
}

func calcDiff(now, then time.Time) float64 {
	return now.Sub(then).Hours() / 24
}

func ReadConfigfile(configfile string) (*Config, bool) {
	cfgdata, err := ioutil.ReadFile(configfile)

	if err != nil {
		loghammer.Panic("Cannot open config file from " + configfile)
	}

	t := Config{}

	err = yaml.Unmarshal([]byte(cfgdata), &t)

	if err != nil {
		loghammer.Panic("Cannot map yml config file to interface, possible syntax error")
		loghammer.Panic(err)
	}

	return &t, true
}

func do() {
	client := http.Client{}

	if !strings.HasSuffix(config.Elasticsearch, "/") {
		config.Elasticsearch += "/"
	}

	url := fmt.Sprintf("%s_cat/indices?v", config.Elasticsearch)
	req, err := client.Get(url)

	if err != nil {
		loghammer.Panic(err)
	}

	content, err := ioutil.ReadAll(req.Body)
	if err != nil {
		loghammer.Panic(err)
	}
	req.Body.Close()

	data := strings.Split(string(content), "\n")
	data = data[1:]
	space := regexp.MustCompile(`\s+`)
	indices := []string{}

	for _, d := range data {
		if len(d) > 0 {
			spdata := strings.Split(strings.TrimSpace(space.ReplaceAllString(d, " ")), " ")
			indices = append(indices, spdata[2])
		}
	}

	for _, cindex := range config.Indices {
		keep := getPeriod(cindex.Keep)
		if !strings.HasSuffix(cindex.Name, "-") {
			cindex.Name += "-"
		}

		findString := ""

		switch {
		case strings.Contains(cindex.Dateformat, "-"):
			findString = fmt.Sprintf(`%s\d{4}-\d{2}-\d{2}`, cindex.Name)
		case strings.Contains(cindex.Dateformat, "."):
			findString = fmt.Sprintf(`%s\d{4}.\d{2}.\d{2}`, cindex.Name)
		}

		if findString != "" {
			re := regexp.MustCompile(findString)

			for _, index := range indices {
				if re.MatchString(index) {
					loghammer.Info(index)
					indexdate := strings.Split(index, cindex.Name)[1]
					t, _ := time.Parse(cindex.Dateformat, indexdate)
					switch t.String() {
					case "0001-01-01 00:00:00 +0000 UTC":
						loghammer.Errorf("Wrong format for %s", cindex.Name)
					default:
						if calcDiff(time.Now(), t) > keep {
							loghammer.Levelf("dele", "Delete %s", index)
							todelete = append(todelete, index)
						} else {
							loghammer.Levelf("keep", "Keep %s", index)
						}
					}
				}
			}
		} else {
			loghammer.Errorf("There's a problem with the format for %s", cindex.Name)
		}
	}

	for _, index := range todelete {
		if !config.Test {
			delrq, err := http.NewRequest("DELETE", fmt.Sprintf("%s%s", config.Elasticsearch, index), nil)
			if err != nil {
				loghammer.Panic(err)
			}
			req, err := client.Do(delrq)
			if err != nil {
				loghammer.Panic(err)
			}
			if req.StatusCode == 200 {
				loghammer.Infof("Deleted %s", index)
			}
		} else {
			loghammer.Infof("Would delete %s", index)
		}
	}

	todelete = []string{}
}

func main() {
	loghammer.New()
	loghammer.AddLevel("dele", "\x1b[1m\x1b[95m", true, true)
	loghammer.AddLevel("keep", "\x1b[1m\x1b[94m", true, true)
	loghammer.AddLevel("schd", "\x1b[1m\x1b[96m", true, true)

	config, ok = ReadConfigfile("config.yml")

	if !ok {
		loghammer.Panic("There was something wrong in the configfile!")
	}

	if config.Logfile != "" {
		loghammer.SetLogFile(config.Logfile)
	}
	switch config.Schedule {
	case "":
		do()
	default:
		loghammer.Levelf("schd", "Running everyday at %s", config.Schedule)
		gocron.Every(1).Day().At(config.Schedule).Do(do)
		<- gocron.Start()
	}
}
